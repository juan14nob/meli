package com.meli.androidtest.model;

import org.json.JSONException;
import org.json.JSONObject;

public class PayerCost {
    private long installments;
    private long installment_rate;
    private long discount_rate;
    private int min_allowed_amount;
    private long max_allowed_amount;
    private String recommended_message;
    private int installment_amount;
    private int total_amount;

    public PayerCost(JSONObject json) throws JSONException {
        this.installments = json.getLong("installments");
        this.installment_rate = json.getLong("installment_rate");
        this.discount_rate = json.getLong("discount_rate");
        this.min_allowed_amount = json.getInt("min_allowed_amount");
        this.max_allowed_amount = json.getLong("max_allowed_amount");
        this.recommended_message = json.getString("recommended_message");
        this.installment_amount = json.getInt("installment_amount");
        this.total_amount = json.getInt("total_amount");
    }

    public long getInstallments() {
        return installments;
    }

    public void setInstallments(long installments) {
        this.installments = installments;
    }

    public long getInstallment_rate() {
        return installment_rate;
    }

    public void setInstallment_rate(long installment_rate) {
        this.installment_rate = installment_rate;
    }

    public long getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(long discount_rate) {
        this.discount_rate = discount_rate;
    }

    public int getMin_allowed_amount() {
        return min_allowed_amount;
    }

    public void setMin_allowed_amount(int min_allowed_amount) {
        this.min_allowed_amount = min_allowed_amount;
    }

    public long getMax_allowed_amount() {
        return max_allowed_amount;
    }

    public void setMax_allowed_amount(long max_allowed_amount) {
        this.max_allowed_amount = max_allowed_amount;
    }

    public String getRecommended_message() {
        return recommended_message;
    }

    public void setRecommended_message(String recommended_message) {
        this.recommended_message = recommended_message;
    }

    public int getInstallment_amount() {
        return installment_amount;
    }

    public void setInstallment_amount(int installment_amount) {
        this.installment_amount = installment_amount;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }
}
