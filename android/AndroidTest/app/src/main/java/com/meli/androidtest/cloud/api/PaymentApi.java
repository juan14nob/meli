package com.meli.androidtest.cloud;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class PaymentApi {

    public void getPaymentsMethods(final Callback callback) throws JSONException {
        Cloud.get("v1/payment_methods", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                callback.onSuccess(response);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                // Do something with the response
                callback.onSuccess(response);
            }
        });
    }


    // https://api.mercadopago.com/v1/payment_methods/card_issuers?public_key=444a9ef5-8a6b-429f-abdf-587639155d88&payment_method_id=visa

    public void getCardIssuers(final Callback callback, String payment_method_id) {
        RequestParams params = new RequestParams();
        params.add("payment_method_id", payment_method_id);

        Cloud.get("v1/payment_methods/card_issuers", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onSuccess(response);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                callback.onSuccess(response);
            }
        });
    }

    // https://api.mercadopago.com/v1/payment_methods/installments?public_key=PUBLIC_KEY&amount=MONTO_DEL_PASO_1&payment_method_id=MEDIO_DE_PAGO_SELECCIONADO&issuer.id=ISSUER_SELECCIONADO​
    public void getInstallments(final Callback callback, Double amount, String payment_method_id, int issuerId) {
        RequestParams params = new RequestParams();
        params.add("payment_method_id", payment_method_id);
        params.add("amount", String.valueOf(amount));
        params.add("issuer.id", String.valueOf(issuerId));

        Cloud.get("v1/payment_methods/installments", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.onSuccess(response);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                callback.onSuccess(response);
            }
        });
    }


}
