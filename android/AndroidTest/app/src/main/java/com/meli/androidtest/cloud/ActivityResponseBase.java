package com.meli.androidtest.cloud;

import com.meli.androidtest.activities.IssuersActivity;
import com.meli.androidtest.model.Installment;

import java.util.ArrayList;

public interface ActivityResponseBase {
    void onResult(ArrayList arrayList);
    void onResult(Installment installment);

}
