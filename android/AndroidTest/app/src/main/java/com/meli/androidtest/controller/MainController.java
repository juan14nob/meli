package com.meli.androidtest.controller;

import com.meli.androidtest.model.Issuer;
import com.meli.androidtest.model.PayerCost;
import com.meli.androidtest.model.PaymentMethod;

/**
 * Created by jcdesign on 30/07/17.
 */

public class MainController {
    private static final MainController instance = new MainController();

    public static MainController getInstance() {
        return instance;
    }

    private Double amount;
    private PaymentMethod paymentMethod;
    private Issuer issuer;



    private PayerCost payerCost;

    private MainController() {

    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public PayerCost getPayerCost() {
        return payerCost;
    }

    public void setPayerCost(PayerCost payerCost) {
        this.payerCost = payerCost;
    }
}
