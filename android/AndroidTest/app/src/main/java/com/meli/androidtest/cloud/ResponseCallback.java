package com.meli.androidtest.cloud;

import com.meli.androidtest.model.Installment;

import java.util.ArrayList;

/**
 * Created by jcdesign on 07/08/17.
 */

public abstract class ResponseCallback implements ActivityResponseBase {

    @Override
    public void onResult(ArrayList arrayList) {

    }

    @Override
    public void onResult(Installment installment) {

    }


}
