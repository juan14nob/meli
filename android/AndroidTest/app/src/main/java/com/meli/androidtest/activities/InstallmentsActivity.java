package com.meli.androidtest.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.meli.androidtest.R;
import com.meli.androidtest.adapters.ListInstallmentAdapter;
import com.meli.androidtest.cloud.ResponseCallback;
import com.meli.androidtest.controller.MainController;
import com.meli.androidtest.controller.PaymentController;
import com.meli.androidtest.model.Installment;
import com.meli.androidtest.model.PayerCost;

import java.util.ArrayList;

public class InstallmentsActivity extends Activity {
    private ListInstallmentAdapter adapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installments);

        listView = (ListView) findViewById(R.id.listView);


        PaymentController paymentController = new PaymentController();

        MainController mainController = MainController.getInstance();

        paymentController.getInstallments(new ResponseCallback() {
            @Override
            public void onResult(final Installment installment) {
                adapter = new ListInstallmentAdapter(getApplicationContext(), buildAdapterData(installment));
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        PayerCost payCost = installment.getPayer_costs().get(position);

                        MainController mainController = MainController.getInstance();
                        mainController.setPayerCost(payCost);

                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("isFinish", true);
                        startActivity(i);
                    }
                });
            }

        }, mainController.getAmount(), mainController.getPaymentMethod().getId(), mainController.getIssuer().getId());

    }

    public ArrayList<ListInstallmentAdapter.AdapterModelData> buildAdapterData(Installment installment) {
        ArrayList<ListInstallmentAdapter.AdapterModelData> data = new ArrayList();

        for(int i = 0; i<installment.getPayer_costs().size(); i++) {
            PayerCost payer = installment.getPayer_costs().get(i);
            data.add(new ListInstallmentAdapter.AdapterModelData(payer.getRecommended_message()));
        }

        return data;
    }
}
