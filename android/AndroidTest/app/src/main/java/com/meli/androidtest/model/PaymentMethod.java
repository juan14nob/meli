package com.meli.androidtest.model;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentMethod {
    public String id;
    public String name;
    public String payment_type_id;
    public String status;
    public String secure_thumbnail;
    public String thumbnail;
    public String deferred_capture;


    public PaymentMethod(JSONObject json) throws JSONException {
        this.id = json.getString("id").toString();
        this.name = json.getString("id").toString();
        this.payment_type_id = json.getString("payment_type_id").toString();
        this.status = json.getString("status").toString();
        this.secure_thumbnail = json.getString("secure_thumbnail").toString();
        this.thumbnail = json.getString("thumbnail").toString();
        this.deferred_capture = json.getString("deferred_capture").toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecure_thumbnail() {
        return secure_thumbnail;
    }

    public void setSecure_thumbnail(String secure_thumbnail) {
        this.secure_thumbnail = secure_thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDeferred_capture() {
        return deferred_capture;
    }

    public void setDeferred_capture(String deferred_capture) {
        this.deferred_capture = deferred_capture;
    }
}
