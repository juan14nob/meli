package com.meli.androidtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.meli.androidtest.R;
import com.meli.androidtest.adapters.ListAdapter;
import com.meli.androidtest.cloud.ActivityResponseBase;
import com.meli.androidtest.cloud.ResponseCallback;
import com.meli.androidtest.controller.MainController;
import com.meli.androidtest.controller.PaymentController;
import com.meli.androidtest.model.Issuer;

import java.util.ArrayList;

public class IssuersActivity extends BaseListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issuers);

        listView = (ListView) findViewById(R.id.listView);

        PaymentController paymentController = new PaymentController();

        MainController mainController = MainController.getInstance();


        paymentController.getCardIssuers(mainController.getPaymentMethod(), new ResponseCallback() {
            @Override
            public void onResult(final ArrayList arrayList) {
                adapter = new ListAdapter(getApplicationContext(), buildAdapterData(arrayList));
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        Issuer issuer = ((ArrayList<Issuer>) arrayList).get(position);

                        MainController mainController = MainController.getInstance();
                        mainController.setIssuer(issuer);

                        startActivity(new Intent(getApplicationContext(), InstallmentsActivity.class));
                    }
                });
            }
        });
    }

    public ArrayList<ListAdapter.AdapterModelData> buildAdapterData(ArrayList<Issuer> arrayList) {
        ArrayList<ListAdapter.AdapterModelData> data = new ArrayList();

        for(int i = 0; i<arrayList.size(); i++) {
            Issuer b = arrayList.get(i);
            data.add(new ListAdapter.AdapterModelData(b.getName(), b.getThumbnail()));
        }

        return data;
    }
}
