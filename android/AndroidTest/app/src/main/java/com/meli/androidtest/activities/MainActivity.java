package com.meli.androidtest.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.meli.androidtest.R;
import com.meli.androidtest.controller.MainController;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Bundle b = getIntent().getExtras();
        if(b != null) {
            if(b.getBoolean("isFinish", false)) {
                MainController mainController = MainController.getInstance();
                // @"Amount: %@\r Payment method: %@ \r Issuer: %@\r Recommended message
                String message = "Amount: "+mainController.getAmount() +
                        "\r\nPayment method: "+ mainController.getPaymentMethod().getName() +
                        "\r\nIssuer: "+ mainController.getIssuer().getName() +
                        "\r\nRecommended message: "+ mainController.getPayerCost().getRecommended_message();

                new AlertDialog.Builder(this)
                        .setTitle("FINISH")
                        .setMessage(message)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }

        }


        Button btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AmountActivity.class);
                startActivity(i);
            }
        });
    }
}
