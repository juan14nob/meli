package com.meli.androidtest.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.meli.androidtest.R;
import com.meli.androidtest.controller.MainController;

public class AmountActivity extends AppCompatActivity {
    EditText et_amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);

        et_amount = (EditText) findViewById(R.id.et_amount);

        Button btn = (Button) findViewById(R.id.btnPay);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_amount.getText().length() > 0) {
                    MainController mainController = MainController.getInstance();
                    mainController.setAmount(Double.valueOf(et_amount.getText().toString()));

                    startActivity(new Intent(getApplicationContext(), PaymentMethodActivity.class));
                }

            }
        });
    }
}
