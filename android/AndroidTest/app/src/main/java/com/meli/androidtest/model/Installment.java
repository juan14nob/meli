package com.meli.androidtest.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jcdesign on 31/07/17.
 */

public class Installment {
    private String payment_method_id;
    private String payment_type_id;
    private String id;
    private String processing_mode;
    private ArrayList<PayerCost> payer_costs;

    public Installment(JSONObject json) throws JSONException {
        this.payment_method_id = json.getString("payment_method_id");
        this.payment_type_id = json.getString("payment_type_id");
        this.processing_mode = json.getString("processing_mode");

        this.payer_costs = new ArrayList<>();
        JSONArray payerCosts = json.getJSONArray("payer_costs");
        for(int i = 0; i<payerCosts.length(); i++)
            this.payer_costs.add(new PayerCost(payerCosts.getJSONObject(i)));
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcessing_mode() {
        return processing_mode;
    }

    public void setProcessing_mode(String processing_mode) {
        this.processing_mode = processing_mode;
    }

    public ArrayList<PayerCost> getPayer_costs() {
        return payer_costs;
    }

    public void setPayer_costs(ArrayList<PayerCost> payer_costs) {
        this.payer_costs = payer_costs;
    }
}
