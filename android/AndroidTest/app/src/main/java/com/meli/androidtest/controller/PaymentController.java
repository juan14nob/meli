package com.meli.androidtest.controller;

import com.meli.androidtest.cloud.ActivityResponseBase;
import com.meli.androidtest.cloud.Callback;
import com.meli.androidtest.cloud.PaymentApi;
import com.meli.androidtest.cloud.ResponseCallback;
import com.meli.androidtest.model.Installment;
import com.meli.androidtest.model.Issuer;
import com.meli.androidtest.model.PaymentMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PaymentController {

    public void getPaymentMethods(final ActivityResponseBase callback) {

        PaymentApi paymentApi = new PaymentApi();
        try {
            paymentApi.getPaymentsMethods(new Callback() {
                @Override
                public void onSuccess(JSONObject json) {

                }

                @Override
                public void onSuccess(JSONArray json) {
                    callback.onResult(makePaymentMethod(json));

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCardIssuers(PaymentMethod paymentMethod, final ActivityResponseBase callback) {
        PaymentApi paymentApi = new PaymentApi();
        paymentApi.getCardIssuers(new Callback() {
            @Override
            public void onSuccess(JSONObject json) {

            }

            @Override
            public void onSuccess(JSONArray json) {
                System.out.println("onSuccess Card issuers");
                System.out.println(json);

                callback.onResult(makeCardIssuers(json));

            }
        }, paymentMethod.getId());

    }

    public void getInstallments(final ResponseCallback callback,
                                Double amount,
                                String payment_method_id,
                                int issuer_id) {
        PaymentApi paymentApi = new PaymentApi();
        paymentApi.getInstallments(new Callback() {
            @Override
            public void onSuccess(JSONObject json) {

            }

            @Override
            public void onSuccess(JSONArray json) {
                System.out.println("onSuccess getInstallments");
                System.out.println(json);

                try {
                    Installment installment = new Installment(json.getJSONObject(0));
                    callback.onResult(installment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, amount, payment_method_id, issuer_id);
    }

    private ArrayList<PaymentMethod> makePaymentMethod(JSONArray data) {
        ArrayList payments = new ArrayList();
        for(int i = 0; i<data.length(); i++) {
            try {
                PaymentMethod payment = new PaymentMethod((JSONObject) data.get(i));
                payments.add(payment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return payments;
    }

    private ArrayList<Issuer> makeCardIssuers(JSONArray data) {
        ArrayList issuers = new ArrayList();
        for(int i = 0; i<data.length(); i++) {
            try {
                Issuer issuer = new Issuer((JSONObject) data.get(i));
                issuers.add(issuer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return issuers;
    }
}
