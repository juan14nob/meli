package com.meli.androidtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.meli.androidtest.R;
import com.meli.androidtest.adapters.ListAdapter;
import com.meli.androidtest.cloud.ActivityResponseBase;
import com.meli.androidtest.cloud.ResponseCallback;
import com.meli.androidtest.controller.MainController;
import com.meli.androidtest.controller.PaymentController;
import com.meli.androidtest.model.PaymentMethod;


import java.util.ArrayList;

public class PaymentMethodActivity extends BaseListActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        listView = (ListView) findViewById(R.id.listView);

        PaymentController paymentController = new PaymentController();

        paymentController.getPaymentMethods(new ResponseCallback() {
            @Override
            public void onResult(final ArrayList arrayList) {


                adapter = new ListAdapter(getApplicationContext(), buildAdapterData(arrayList));
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        PaymentMethod p = ((ArrayList<PaymentMethod>) arrayList).get(position);

                        MainController mainController = MainController.getInstance();
                        mainController.setPaymentMethod(p);

                        startActivity(new Intent(getApplicationContext(), IssuersActivity.class));
                    }
                });

            }
        });




    }

    public ArrayList<ListAdapter.AdapterModelData> buildAdapterData(ArrayList<PaymentMethod> arrayList) {
        ArrayList<ListAdapter.AdapterModelData> data = new ArrayList();

        for(int i = 0; i<arrayList.size(); i++) {
            PaymentMethod paymentMethod = arrayList.get(i);
            data.add(new ListAdapter.AdapterModelData(paymentMethod.getName(), paymentMethod.getThumbnail()));
        }

        return data;
    }


}
