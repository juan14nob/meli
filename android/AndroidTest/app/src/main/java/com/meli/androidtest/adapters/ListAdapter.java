package com.meli.androidtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meli.androidtest.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListAdapter extends BaseAdapter {
    private Context context;

    private List<AdapterModelData> items;

    public ListAdapter(Context context, List<AdapterModelData> items) {
        this.context = context;
        this.items  = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView tvTitle;
        ImageView iv;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item, parent, false);

            tvTitle = (TextView) convertView.findViewById(R.id.tv_payment_method);
            iv = (ImageView) convertView.findViewById(R.id.iv_payment_method);
            convertView.setTag(new ViewHolder(iv, tvTitle));
        } else {

            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tvTitle = viewHolder.name;
            iv = viewHolder.image;
        }

        AdapterModelData item = this.items.get(position);

        tvTitle.setText(item.getTitle());
        //new ImageTask(iv).execute(item.getThumb());
        Picasso.with(context).load(item.getThumb()).fit().centerCrop()
                .placeholder(R.mipmap.placeholder)
                .into(iv);


        return convertView;
    }


    public static class AdapterModelData {

        String title;
        String thumb;

        public AdapterModelData(String title, String thumb) {
            this.title = title;
            this.thumb = thumb;
        }
        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumb() {
            return this.thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

    }

    private static class ViewHolder {
        public final ImageView image;
        public final TextView name;

        public ViewHolder(ImageView img, TextView name) {
            this.image = img;
            this.name= name;
        }
    }

}



