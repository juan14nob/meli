package com.meli.androidtest.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Issuer {

    private int id;
    private String name;
    private String secure_thumbnail;
    private String thumbnail;
    private String processing_mode;
    private String merchant_account_id;

    public Issuer(JSONObject json) throws JSONException {


        if(json.get("id") != null) {
            this.id = json.getInt("id");
        }

        if(json.get("name") != null) {
            this.name = json.getString("name");
        }

        if(json.get("secure_thumbnail") != null) {
            this.secure_thumbnail = json.getString("secure_thumbnail");
        }

        if(json.get("thumbnail") != null) {
            this.thumbnail = json.getString("thumbnail");
        }

        if(json.get("processing_mode") != null) {
            this.processing_mode = json.getString("processing_mode");
        }

        if(json.get("merchant_account_id") != null) {
            this.merchant_account_id = json.getString("merchant_account_id");
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecure_thumbnail() {
        return secure_thumbnail;
    }

    public void setSecure_thumbnail(String secure_thumbnail) {
        this.secure_thumbnail = secure_thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProcessing_mode() {
        return processing_mode;
    }

    public void setProcessing_mode(String processing_mode) {
        this.processing_mode = processing_mode;
    }

    public String getMerchant_account_id() {
        return merchant_account_id;
    }

    public void setMerchant_account_id(String merchant_account_id) {
        this.merchant_account_id = merchant_account_id;
    }


}
