package com.meli.androidtest.cloud;

import com.loopj.android.http.*;

/**
 * Created by jcdesign on 29/07/17.
 */

public class Cloud {
    public static final String public_key = "444a9ef5-8a6b-429f-abdf-587639155d88";
    private static final String BASE_URL = "https://api.mercadopago.com/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        System.out.println("GET URL "+getAbsoluteUrl(url));
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl + "?public_key="+public_key;
    }
}
