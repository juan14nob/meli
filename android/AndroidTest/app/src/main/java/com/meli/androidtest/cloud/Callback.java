package com.meli.androidtest.cloud;

import org.json.JSONArray;
import org.json.JSONObject;

public interface Callback {
    public void onSuccess(JSONObject j);
    public void onSuccess(JSONArray j);
}
