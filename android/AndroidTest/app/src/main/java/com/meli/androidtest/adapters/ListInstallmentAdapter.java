package com.meli.androidtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meli.androidtest.R;
import com.meli.androidtest.view.ImageTask;

import java.util.List;

public class ListInstallmentAdapter extends BaseAdapter {
    private Context context;

    private List<AdapterModelData> items;

    public ListInstallmentAdapter(Context context, List<AdapterModelData> items) {
        this.context = context;
        this.items  = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView tvTitle;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_installment, parent, false);

            tvTitle = (TextView) convertView.findViewById(R.id.tv_payment_method);
            convertView.setTag(new ViewHolder(tvTitle));
        } else {

            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tvTitle = viewHolder.name;
        }

        AdapterModelData item = this.items.get(position);

        tvTitle.setText(item.getTitle());
        return convertView;
    }


    public static class AdapterModelData {
        String title;

        public AdapterModelData(String title) {
            this.title = title;
        }
        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    private static class ViewHolder {
        public final TextView name;

        public ViewHolder(TextView name) {
            this.name= name;
        }
    }

}



