package com.meli.androidtest.activities;

import android.app.Activity;
import android.widget.ListView;

import com.meli.androidtest.adapters.ListAdapter;
import com.meli.androidtest.adapters.ListInstallmentAdapter;

public class BaseListActivity extends Activity {
    ListView listView;
    ListAdapter adapter;
}
