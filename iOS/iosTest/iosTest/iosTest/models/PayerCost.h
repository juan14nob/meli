//
//  PayerCost.h
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"

@interface PayerCost : Model {
    NSDictionary * dictionaryPayer;
}

@property (nonatomic, strong) NSNumber * installments;
@property (nonatomic, strong) NSNumber * installment_rate;
@property (nonatomic, strong) NSNumber * discount_rate;
@property (nonatomic, strong) NSNumber * min_allowed_amount;
@property (nonatomic, strong) NSNumber * max_allowed_amount;
@property (nonatomic, strong) NSString * recommended_message;
@property (nonatomic, strong) NSNumber * installment_amount;
@property (nonatomic, strong) NSNumber * total_amount;


@end
