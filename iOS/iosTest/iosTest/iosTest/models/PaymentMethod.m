//
//  PaymentMethod.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "PaymentMethod.h"

@implementation PaymentMethod

-(id) initWithDictionary:(NSDictionary *) dictionary {
    dictionaryPayment = dictionary;
    
    [self setName:dictionary[@"name"]];
    [self setStatus:dictionary[@"status"]];
    [self setId:dictionary[@"id"]];
    [self setThumbnail:dictionary[@"thumbnail"]];
    [self setPayment_type_id:dictionary[@"payment_type_id"]];
    [self setDeferred_capture:dictionary[@"deferred_capture"]];
    [self setSecure_thumbnail:dictionary[@"secure_thumbnail"]];
    
    return self;

}

@end
