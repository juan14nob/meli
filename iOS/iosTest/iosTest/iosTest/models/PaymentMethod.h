//
//  PaymentMethod.h
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"

@interface PaymentMethod : Model {
    NSDictionary * dictionaryPayment;
}

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * payment_type_id;

@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * secure_thumbnail;
@property (nonatomic, strong) NSString * thumbnail;
@property (nonatomic, strong) NSString * deferred_capture;


@end
