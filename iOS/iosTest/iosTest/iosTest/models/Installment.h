//
//  Installment.h
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"

@interface Installment : Model{
    NSDictionary * dictionaryInstallment;
}

@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * payment_method_id;
@property (nonatomic, strong) NSString * payment_type_id;
@property (nonatomic, strong) NSString * processing_mode;
@property (nonatomic, strong) NSMutableArray * payer_costs; // Array of PayerCost Objects
@property (nonatomic, strong) NSString * merchant_account_id;

@end
