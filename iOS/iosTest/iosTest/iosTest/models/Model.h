//
//  Model.h
//  iosTest
//
//  Created by WebeeDevs on 8/4/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject



@end

@interface Model (Protected)

-(id) initWithDictionary:(NSDictionary *) dictionary;

@end
