//
//  Issuer.h
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"

@interface Issuer : Model {
    NSDictionary * dictionaryIssuer;
}


@property (nonatomic, strong) NSNumber * id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * secure_thumbnail;
@property (nonatomic, strong) NSString * thumbnail;
@property (nonatomic, strong) NSString * processing_mode;
@property (nonatomic, strong) NSString * merchant_account_id;


@end
