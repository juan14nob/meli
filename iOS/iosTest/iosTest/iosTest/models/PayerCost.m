//
//  PayerCost.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "PayerCost.h"

@implementation PayerCost


-(id)initWithDictionary:(NSDictionary *)dictionary {
    [self setInstallments:[NSNumber numberWithLong:[dictionary[@"installments"] longValue]]];
    [self setTotal_amount:[NSNumber numberWithLong:[dictionary[@"total_amount"] longValue]]];
    [self setDiscount_rate:[NSNumber numberWithLong:[dictionary[@"discount_rate"] longValue]]];
    [self setInstallment_amount:[NSNumber numberWithLong:[dictionary[@"installment_amount"] longValue]]];
    [self setInstallment_rate:[NSNumber numberWithLong:[dictionary[@"installment_rate"] longValue]]];
    
    [self setMax_allowed_amount:[NSNumber numberWithLong:[dictionary[@"max_allowed_amount"] longValue]]];
    [self setMin_allowed_amount:[NSNumber numberWithLong:[dictionary[@"min_allowed_amount"] longValue]]];
    [self setRecommended_message:dictionary[@"recommended_message"]];
    
    
    return self;
}


@end
