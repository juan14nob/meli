//
//  Issuer.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "Issuer.h"

@implementation Issuer

-(Issuer *) initWithDictionary:(NSDictionary *) dictionary {
    dictionaryIssuer = dictionary;
    
    [self setId:[NSNumber numberWithInt:[dictionary[@"id"] intValue]]];
    [self setName:dictionary[@"name"]];
    [self setProcessing_mode:dictionary[@"processing_mode"]];
    [self setThumbnail:dictionary[@"thumbnail"]];
    [self setMerchant_account_id:dictionary[@"merchant_account_id"]];
    [self setSecure_thumbnail:dictionary[@"secure_thumbnail"]];
    
    return self;
}

@end
