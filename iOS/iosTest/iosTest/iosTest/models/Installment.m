//
//  Installment.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "Installment.h"
#import "PayerCost.h"

@implementation Installment


-(Installment *)initWithDictionary:(NSDictionary *)dictionary {
    dictionaryInstallment = dictionary;
    
    [self setPayment_type_id:dictionary[@"payment_type_id"]];
    [self setProcessing_mode:dictionary[@"processing_mode"]];
    [self setMerchant_account_id:dictionary[@"merchant_account_id"]];
    
    NSMutableArray * payerCosts = [NSMutableArray new];
    for(NSDictionary *_payerCost in dictionary[@"payer_costs"])
        [payerCosts addObject:[[PayerCost alloc] initWithDictionary:_payerCost]];
    
    [self setPayer_costs:payerCosts];
    
    return self;
}

@end
