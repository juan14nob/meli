//
//  Main.h
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "Issuer.h"
#import "PayerCost.h"

@interface MainController : NSObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) PaymentMethod * paymentMethodSelected;
@property (nonatomic, retain) Issuer * issuerSelected;
@property (nonatomic, retain) PayerCost * payerCostSelected;

+ (id)sharedInstance;

@end
