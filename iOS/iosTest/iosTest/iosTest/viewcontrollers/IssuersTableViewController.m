//
//  IssuersTableViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "IssuersTableViewController.h"
#import "CloudManager+PaymentAPI.h"
#import "PaymentMethod.h"

#import "UIImageView+AFNetworking.h"

#import "MainController.h"

@interface IssuersTableViewController ()

@end

@implementation IssuersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self callAPI];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) callAPI {
    
    MainController * main = [MainController sharedInstance];
    
    NSString * paymentIdSelected = main.paymentMethodSelected.id;
    
    [[CloudManager sharedInstance] getCardIssuersWithPaymentId:paymentIdSelected
                                                  withCallback:^(NSMutableArray *models) {
                                                      
                                                      self.data = models;
                                                      [self.tableView reloadData];
                                                      
                                                  }];
    
}

-(void) set:(UIImageView *) image
      byUrl:(NSString *) url {
    
    [image setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Issuer * issuer = self.data[indexPath.row];
    
    [[MainController sharedInstance] setIssuerSelected:issuer];
    
    [self performSegueWithIdentifier:@"InstallmentsSegue" sender:self];
    
}

@end
