//
//  InstallmentsTableViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "InstallmentsTableViewController.h"
#import "MainViewController.h"
#import "CloudManager+PaymentAPI.h"
#import "PaymentMethod.h"
#import "PayerCost.h"

#import "UIImageView+AFNetworking.h"

#import "MainController.h"

#define kFinishSegue @"finishSegue"

@interface InstallmentsTableViewController ()

@property Installment * installmentData;

@end

@implementation InstallmentsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self callAPI];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) callAPI {
    
    MainController * main = [MainController sharedInstance];
    
    NSString * paymentIdSelected = main.paymentMethodSelected.id;
    NSString * issuerId = [main.issuerSelected.id stringValue];
    
    [[CloudManager sharedInstance] getInstallmentsWithAmount:main.amount
                                               withPaymentId:paymentIdSelected
                                                withIssuerId:issuerId
                                                withCallback:^(Installment *installment) {
                                                    self.installmentData = installment;
                                                    [self.tableView reloadData];
                                                }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.installmentData.payer_costs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    PayerCost *payerCost = self.installmentData.payer_costs[indexPath.row];
    
    UILabel *message = [cell viewWithTag:1];
    message.text = payerCost.recommended_message;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PayerCost * payerCostSelected = self.installmentData.payer_costs[indexPath.row];
    
    [[MainController sharedInstance] setPayerCostSelected:payerCostSelected];
    
    [self performSegueWithIdentifier:kFinishSegue sender:self];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    MainViewController *vc = [segue destinationViewController];
    vc.isFinish = YES;
    
    
}


@end
