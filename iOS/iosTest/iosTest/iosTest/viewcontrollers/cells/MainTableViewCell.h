//
//  MainTableViewCell.h
//  iosTest
//
//  Created by WebeeDevs on 8/4/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property(assign, nonatomic) IBOutlet UILabel * title;
@property(assign, nonatomic) IBOutlet UIImageView * image;

@end
