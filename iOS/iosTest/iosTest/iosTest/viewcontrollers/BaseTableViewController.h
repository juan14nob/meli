//
//  BaseTableViewController.h
//  iosTest
//
//  Created by WebeeDevs on 8/4/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainController.h"

@interface BaseTableViewController : UITableViewController
@property NSMutableArray * data;
@end
