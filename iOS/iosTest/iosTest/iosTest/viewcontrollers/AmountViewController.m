//
//  AmountViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "AmountViewController.h"
#import "MainController.h"

@interface AmountViewController ()
@property(assign, nonatomic) IBOutlet UITextField * amountTf;
@end

@implementation AmountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if(self.amountTf.text.length > 0) {
        MainController *main = [MainController sharedInstance];
        main.amount = [NSNumber numberWithInt:[self.amountTf.text intValue]];
        
        return YES;
    }else{
        [self showErrorMessage];
    }
    return NO;
}


-(void) showErrorMessage {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:@"Please, set amount"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action){}];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
