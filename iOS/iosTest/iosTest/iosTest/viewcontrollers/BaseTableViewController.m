//
//  BaseTableViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/4/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "BaseTableViewController.h"
#import "MainTableViewCell.h"

#import "UIImageView+AFNetworking.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) set:(UIImageView *) image
      byUrl:(NSString *) url {
    
    [image setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
    if (cell == nil) {
        NSArray *objectCells = [[NSBundle mainBundle] loadNibNamed:@"MainTableViewCell" owner:self options:nil];
        
        cell = [objectCells objectAtIndex:0];
    }
    
    NSString * title;
    NSString * thumb;
    
    if([self.data[indexPath.row] isKindOfClass:[PaymentMethod class]]) {
        PaymentMethod * payment = self.data[indexPath.row];
        title = payment.id;
        thumb = payment.thumbnail;
        
    } else {
        Issuer * issuer = self.data[indexPath.row];
        title = issuer.name;
        thumb = issuer.thumbnail;
    }
    
    
    cell.title.text = title;
    
    [self set:cell.image
        byUrl:thumb];
    
    return cell;
}


@end
