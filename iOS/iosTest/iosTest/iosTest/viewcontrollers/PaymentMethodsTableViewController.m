//
//  PaymentMethodsTableViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/3/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "PaymentMethodsTableViewController.h"
#import "CloudManager+PaymentAPI.h"

#import "MainController.h"

#define kCardIssuersSegue @"goToCardIssuersIdentifier"

@interface PaymentMethodsTableViewController ()

@end

@implementation PaymentMethodsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self callAPI];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) callAPI {
    
    [[CloudManager sharedInstance] getPaymentMethods:^(NSMutableArray *models) {
        self.data = models;
        [self.tableView reloadData];
    }];    
    
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentMethod * payment = self.data[indexPath.row];
    
    [[MainController sharedInstance] setPaymentMethodSelected:payment];
    
    [self performSegueWithIdentifier:kCardIssuersSegue sender:self];
}

@end
