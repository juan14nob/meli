//
//  ViewController.m
//  iosTest
//
//  Created by WebeeDevs on 8/2/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "MainViewController.h"
#import "MainController.h"


@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"Main view controller");
    
    if(_isFinish) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [self showAlertMessage];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showAlertMessage {
    
    MainController *main = [MainController sharedInstance];
    
    NSString * message = [NSString stringWithFormat:@"Amount: %@\r Payment method: %@ \r Issuer: %@\r Recommended message: %@", main.amount, main.paymentMethodSelected.name, main.issuerSelected.name, main.payerCostSelected.recommended_message];
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"FINISH"
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end
