//
//  Payment.m
//  iosTest
//
//  Created by WebeeDevs on 8/2/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "CloudManager+PaymentAPI.h"
#import "PaymentMethod.h"
#import "Issuer.h"

static NSString * kPayment = @"v1/payment_methods";

@implementation CloudManager (PaymentAPI)


-(void) getPaymentMethods:(modelData) callback {
    
    NSMutableArray * models = [NSMutableArray new];
    
    [self call:kPayment
        withParameters: [NSArray new]
          withCallback:^(BOOL status, NSDictionary * value) {
          
    } andCallbackValues:^(BOOL status, NSMutableArray * values) {
        
        for(NSDictionary *json in values) {
            PaymentMethod *paymentMethod = [[PaymentMethod alloc] initWithDictionary:json];
            [models addObject:paymentMethod];
        }
        
        callback(models);
    
    }];
    
}

-(void) getCardIssuersWithPaymentId:(NSString *) paymentId
                       withCallback:(modelData) callback {
    NSMutableArray * issuers = [NSMutableArray new];
    
    NSString *paymentIdParameter = [NSString stringWithFormat:@"payment_method_id=%@", paymentId];

    [self call:[NSString stringWithFormat:@"%@/%@", kPayment, @"card_issuers"]
        withParameters: @[paymentIdParameter]
          withCallback:^(BOOL status, NSDictionary * value) {
              
          } andCallbackValues:^(BOOL status, NSMutableArray * values) {
              
              for(NSDictionary *json in values) {
                  Issuer *issuer = [[Issuer alloc] initWithDictionary:json];
                  [issuers addObject:issuer];
              }
              
              callback(issuers);
          }];

}

-(void) getInstallmentsWithAmount:(NSNumber *) amount
                    withPaymentId:(NSString *) payment_method_id
                     withIssuerId:(NSString *) issuerId
                     withCallback:(callbackInstallments) callback {
    
    NSMutableArray * installments = [NSMutableArray new];
    
    NSString *paymentIdParameter = [NSString stringWithFormat:@"payment_method_id=%@", payment_method_id];
    NSString *amountParameter = [NSString stringWithFormat:@"amount=%@", amount];
    NSString *issuerIdParameter = [NSString stringWithFormat:@"issuer.id=%@", issuerId];
    
    [self call:[NSString stringWithFormat:@"%@/%@", kPayment, @"installments"]
            withParameters:@[paymentIdParameter,amountParameter,issuerIdParameter]
            withCallback:^(BOOL status, NSDictionary *value) {
      
            } andCallbackValues:^(BOOL status, NSMutableArray *values) {
                
                for(NSDictionary *json in values) {
                    Installment *installment = [[Installment alloc] initWithDictionary:json];
                    [installments addObject:installment];
                    
                    callback(installment);
                }
    
            }];
    
    

    
}




@end
