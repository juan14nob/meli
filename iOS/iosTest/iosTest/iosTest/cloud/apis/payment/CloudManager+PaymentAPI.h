//
//  Payment.h
//  iosTest
//
//  Created by WebeeDevs on 8/2/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "CloudManager.h"
#import "Installment.h"

typedef void(^callbackInstallments)(Installment * installment);

@interface CloudManager (PaymentAPI)

-(void) getPaymentMethods:(modelData) callback;

-(void) getCardIssuersWithPaymentId:(NSString *) paymentId
                       withCallback:(modelData) callback;

-(void) getInstallmentsWithAmount:(NSNumber *) amount
                    withPaymentId:(NSString *) payment_method_id
                     withIssuerId:(NSString *) issuerId
                     withCallback:(callbackInstallments) callback;



@end
