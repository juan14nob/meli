//
//  CloudManager.m
//  iosTest
//
//  Created by WebeeDevs on 8/2/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "CloudManager.h"

static NSString *baseUrl = @"https://api.mercadopago.com";
static NSString *publicKey = @"444a9ef5-8a6b-429f-abdf-587639155d88";

@implementation CloudManager

+(CloudManager *)sharedInstance
{
    static CloudManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

-(void) call:(NSString *) method
withParameters:(NSArray *) params
withCallback:(callbackDictionary)value
andCallbackValues:(callbackDictionaryArray)values {
    
    NSString * parameters = @"";
    if(params) {
        parameters = [NSString stringWithFormat:@"&%@", [params componentsJoinedByString:@"&"]];
    }
    
    NSString * urlStr = [NSString stringWithFormat:@"%@/%@?public_key=%@%@", baseUrl, method, publicKey, parameters];
    NSLog(@"%@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *cloudTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data.length > 0 && error == nil) {
            
            id response = [NSJSONSerialization JSONObjectWithData:data
                                                          options:0
                                                            error:NULL];
            
            if ([response isKindOfClass:[NSArray class]]) {
                values(YES, response);
            } else {
                value(YES, response);
            }
        
        }
        
        
    }];
                                      
    [cloudTask resume];
    
}

@end
