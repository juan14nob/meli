//
//  CloudManager.h
//  iosTest
//
//  Created by WebeeDevs on 8/2/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^callbackDictionary)(BOOL status, NSDictionary* value);
typedef void(^callbackDictionaryArray)(BOOL status, NSMutableArray* values);

typedef void(^modelData)(NSMutableArray * models);

@interface CloudManager : NSObject

+(CloudManager *)sharedInstance;

-(void) call:(NSString *) method
withParameters:(NSArray *) params
withCallback:(callbackDictionary)value
andCallbackValues:(callbackDictionaryArray)values;

@end
