//
//  PrimaryButton.m
//  iosTest
//
//  Created by WebeeDevs on 8/4/17.
//  Copyright © 2017 juancarmena. All rights reserved.
//

#import "PrimaryButton.h"

#define KWUBorderColor [UIColor colorWithRed:73.0f/255.0f green:193.0f/255.0f blue:193.0f/255.0f alpha:1]

@implementation PrimaryButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.borderWidth = 1;
    self.layer.borderColor = KWUBorderColor.CGColor;
    
    self.layer.cornerRadius = 8;
    [self.layer setMasksToBounds:YES];
}

@end
